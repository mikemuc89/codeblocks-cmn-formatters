/* @flow */
const IBAN_REGEX = /^([A-Z]{2})?([0-9]{2})([0-9A-Z]+)/;

const sanitize = (value: string) => value.replace(/ /g, '');

const format = (
  value: string,
  {
    defaultValue = '-',
    separator = ' '
  }: {
    defaultValue?: string,
    separator?: string
  } = {}
) => {
  const sanitized = sanitize(value);
  if (!sanitized) {
    return defaultValue;
  }
  const match = sanitized.match(IBAN_REGEX);
  if (match) {
    const [_, countryCode = '', controlDigits, rest] = match;
    return `${countryCode}${controlDigits}${separator}${rest.match(/.{1,4}/g).join(separator)}`;
  }
  return value;
};

type FormatType = (value: string, options?: { defaultValue?: string, separator?: string }) => string;
type SanitizeType = (value: string) => string;

export default (
  Object.assign(format, {
    sanitize
  }): FormatType & {
    sanitize: SanitizeType
  }
);
