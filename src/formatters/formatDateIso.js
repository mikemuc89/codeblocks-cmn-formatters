/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

export default (
  value:
    | string
    | {
        date: string,
        time: string
      }
    | Date
    | null,
  {
    defaultValue = '',
    showTime
  }: {
    defaultValue?: string,
    showTime?: boolean
  } = {}
) => {
  if (value === null) {
    return defaultValue;
  }
  const date =
    value instanceof Date
      ? value
      : typeof value === 'object'
      ? new Date(`${value.date}T${value.time}`)
      : new Date(value);
  return `${padString(date.getFullYear().toString(), 4)}-${padString((date.getMonth() + 1).toString(), 2)}-${padString(
    date.getDate().toString(),
    2
  )}${showTime ? ` ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}` : ''}`;
};
