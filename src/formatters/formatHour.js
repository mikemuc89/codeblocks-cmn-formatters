/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

type DataType = void | number | string;

export default (
  {
    hour,
    minute
  }: {
    hour?: ?DataType,
    minute?: ?DataType
  },
  {
    defaultValue = '-'
  }: {
    defaultValue?: string
  } = {}
) => {
  if ([null, undefined].includes(hour) && [null, undefined].includes(minute)) {
    return defaultValue;
  }
  const defaultPartValue = '00';
  return `${padString(hour === null ? defaultPartValue : hour || '00', 2)}:${padString(
    minute === null ? defaultPartValue : minute || '00',
    2
  )}`;
};
