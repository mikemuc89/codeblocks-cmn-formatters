/* @flow */
export default (value: boolean) => (value ? 'Tak' : 'Nie');
