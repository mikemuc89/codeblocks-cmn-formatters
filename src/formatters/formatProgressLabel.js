/* @flow */
import { PROGRESS_LABEL_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { getPercent } from '@omnibly/codeblocks-cmn-utils';

export default ({
  labelType,
  max,
  min,
  value
}: {
  labelType: $Values<typeof PROGRESS_LABEL_TYPES>,
  max: number,
  min: number,
  value: number
}) =>
  ({
    [PROGRESS_LABEL_TYPES.ABSOLUTE]: () => `${value}`,
    [PROGRESS_LABEL_TYPES.ABSOLUTE_FRACTION]: () => `${value} / ${max}`,
    [PROGRESS_LABEL_TYPES.NONE]: () => '',
    [PROGRESS_LABEL_TYPES.PERCENT]: () => `${getPercent({ max, min, value })}%`
  }[labelType]());
