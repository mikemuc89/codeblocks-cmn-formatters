/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

export default ({ week, year }: { week: string | number, year: string | number }) =>
  `${padString(year, 4, '0')}-W${week}`;
