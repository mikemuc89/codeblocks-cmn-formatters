/* @flow */
import { WEEKDAYS } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (weekDay: $Values<typeof WEEKDAYS>) =>
  ({
    [WEEKDAYS.SUNDAY]: 'niedziela',
    [WEEKDAYS.MONDAY]: 'poniedziałek',
    [WEEKDAYS.TUESDAY]: 'wtorek',
    [WEEKDAYS.WEDNESDAY]: 'środa',
    [WEEKDAYS.THURSDAY]: 'czwartek',
    [WEEKDAYS.FRIDAY]: 'piątek',
    [WEEKDAYS.SATURDAY]: 'sobota'
  }[weekDay]);
