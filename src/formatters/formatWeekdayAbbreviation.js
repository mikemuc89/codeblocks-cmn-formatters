/* @flow */
import { WEEKDAYS } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (weekDay: $Values<typeof WEEKDAYS>) =>
  ({
    [WEEKDAYS.SUNDAY]: 'Nd',
    [WEEKDAYS.MONDAY]: 'Po',
    [WEEKDAYS.TUESDAY]: 'Wt',
    [WEEKDAYS.WEDNESDAY]: 'Śr',
    [WEEKDAYS.THURSDAY]: 'Cz',
    [WEEKDAYS.FRIDAY]: 'Pt',
    [WEEKDAYS.SATURDAY]: 'So'
  }[weekDay]);
