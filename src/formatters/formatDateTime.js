/* @flow */
import formatDate from './formatDate';
import formatDateIso from './formatDateIso';
import formatTime from './formatTime';

export default (
  value: {
    date?: string | Date | null,
    time?: string
  } | null,
  {
    defaultValue = '-'
  }: {
    defaultValue?: string
  } = {}
) => {
  if (value === null) {
    return defaultValue;
  }
  const { date, time } = value;
  if (!date) {
    return formatTime(time);
  }
  const dateObject = new Date(`${formatDateIso(date)}${time ? `T${formatTime(time)}` : ''}`);

  return formatDate(dateObject, { showTime: Boolean(time) });
};
