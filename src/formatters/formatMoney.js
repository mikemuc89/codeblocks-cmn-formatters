/* @flow */
import formatAmount from './formatAmount';

export default ({ amount, currency }: { amount: string | number, currency: string }) =>
  `${formatAmount(amount)}\xa0${currency}`;
