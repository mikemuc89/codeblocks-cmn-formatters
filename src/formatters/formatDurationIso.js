/* @flow */
type ParamType = string | number;

export default ({
  days,
  hours,
  minutes,
  months,
  seconds,
  years
}: {
  days?: ParamType,
  hours?: ParamType,
  minutes?: ParamType,
  months?: ParamType,
  seconds?: ParamType,
  years?: ParamType
}) =>
  `P${years ? `${years}Y` : ''}${months ? `${months}M` : ''}${days ? `${days}D` : ''}T${hours ? `${hours}H` : ''}${
    minutes ? `${minutes}M` : ''
  }${seconds ? `${seconds}S` : ''}`;
