/* @flow */
export default (
  value: string | number,
  {
    defaultValue = '-'
  }: {
    defaultValue: string
  } = {}
) => {
  const parsed = parseFloat(value);
  if (isNaN(parsed)) {
    return defaultValue;
  }
  return parsed.toFixed(2);
};
