/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

export default ({ month, year }: { month: string | number, year: string | number }) =>
  `${padString(year, 4, '0')}-${padString(month, 2, '0')}`;
