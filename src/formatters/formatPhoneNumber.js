/* @flow */
const sanitize = (
  value: string = '',
  {
    separator = ' '
  }: {
    separator: string
  } = {}
) => (value ? value.replace(new RegExp(separator, 'gi'), '') : '');

const format = (
  value: string = '',
  {
    countryCode,
    defaultValue = '',
    groupLength = 3,
    separator = ' '
  }: {
    countryCode?: string | number,
    defaultValue?: string,
    groupLength?: number,
    separator?: string
  } = {}
) => {
  if (!value) {
    return defaultValue;
  }
  const sanitized = sanitize(value, { separator });
  const len = sanitized.length;
  const leftover = len % groupLength;
  const split = (val: string) => val.match(new RegExp(`.{1,${groupLength}}`, 'g')) || [];

  const groups = leftover ? [value.slice(0, leftover), ...split(value.slice(leftover))] : split(value);

  return `${countryCode ? `+${countryCode} ` : ''}${groups.join(separator)}`;
};

export default Object.assign(format, {
  sanitize
});
