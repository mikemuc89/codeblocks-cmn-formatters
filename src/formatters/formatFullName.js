/* @flow */
export default ({ firstName, lastName }: { firstName: string, lastName: string }) => [firstName, lastName].join('\xa0');
