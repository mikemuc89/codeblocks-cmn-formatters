/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

export default ({ day, month }: { day: string | number, month: string | number }) =>
  `${padString(month, 2, '0')}-${padString(day, 2, '0')}`;
