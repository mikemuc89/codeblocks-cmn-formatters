/* @flow */
import { STREET_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (
  value: $Values<typeof STREET_TYPES>,
  {
    defaultValue = '-'
  }: {
    defaultValue?: string
  } = {}
) =>
  ({
    [STREET_TYPES.ALLEY]: 'aleja',
    [STREET_TYPES.ALLEYS]: 'Aleje',
    [STREET_TYPES.BOULEVARD]: 'bulwar',
    [STREET_TYPES.BOULEVARDS]: 'Bulwary',
    [STREET_TYPES.COAST]: 'wybrzeże',
    [STREET_TYPES.COLONY]: 'kolonia',
    [STREET_TYPES.ESTATE]: 'osiedle',
    [STREET_TYPES.GARDEN]: 'ogród',
    [STREET_TYPES.GARDENS]: 'Ogrody',
    [STREET_TYPES.HIGHWAY]: 'szosa',
    [STREET_TYPES.ISLAND]: 'wyspa',
    [STREET_TYPES.LANE]: 'uliczka',
    [STREET_TYPES.MARKET]: 'rynek',
    [STREET_TYPES.OTHER]: '',
    [STREET_TYPES.PARK]: 'park',
    [STREET_TYPES.PLACE]: 'plac',
    [STREET_TYPES.ROAD]: 'droga',
    [STREET_TYPES.ROUNDABOUT]: 'rondo',
    [STREET_TYPES.SQUARE]: 'skwer',
    [STREET_TYPES.STREET]: 'ulica'
  }[value] || defaultValue);
