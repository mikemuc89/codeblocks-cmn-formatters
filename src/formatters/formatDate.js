/* @flow */
import { padString } from '@omnibly/codeblocks-cmn-utils';

export default (
  value: string | Date | null,
  {
    defaultValue = '-',
    showSeconds,
    showTime
  }: {
    defaultValue?: string,
    showSeconds?: boolean,
    showTime?: boolean
  } = {}
) => {
  if (value === null) {
    return defaultValue;
  }
  const date = value instanceof Date ? value : new Date(value);
  return `${padString(date.getDate().toString(), 2)}.${padString((date.getMonth() + 1).toString(), 2)}.${padString(
    date.getFullYear().toString(),
    4
  )}${showTime ? ` ${date.getHours()}:${date.getMinutes()}${showSeconds ? `:${date.getSeconds()}` : ''}` : ''}`;
};
