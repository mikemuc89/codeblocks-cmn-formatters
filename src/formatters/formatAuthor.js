/* @flow */
export default ({ username }: { username: string }) => username;
