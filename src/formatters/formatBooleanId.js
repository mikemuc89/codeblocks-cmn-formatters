/* @flow */
import { BOOL_IDS } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (value: $Values<typeof BOOL_IDS>) =>
  ({
    [BOOL_IDS.YES]: 'Tak',
    [BOOL_IDS.NO]: 'Nie'
  }[value]);
