/* @flow */
import formatMonthName from './formatMonthName';

export default ({ month, year }: { month: string | number, year?: string | number }) =>
  `${formatMonthName(month)} ${year}`;
