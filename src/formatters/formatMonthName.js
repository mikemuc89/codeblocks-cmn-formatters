/* @flow */
import { MONTHS } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (month: $Values<typeof MONTHS>) =>
  ({
    [MONTHS.JANUARY]: 'styczeń',
    [MONTHS.FEBRUARY]: 'luty',
    [MONTHS.MARCH]: 'marzec',
    [MONTHS.APRIL]: 'kwiecień',
    [MONTHS.MAY]: 'maj',
    [MONTHS.JUNE]: 'czerwiec',
    [MONTHS.JULY]: 'lipiec',
    [MONTHS.AUGUST]: 'sierpień',
    [MONTHS.SEPTEMBER]: 'wrzesień',
    [MONTHS.OCTOBER]: 'październik',
    [MONTHS.NOVEMBER]: 'listopad',
    [MONTHS.DECEMBER]: 'grudzień'
  }[month]);
