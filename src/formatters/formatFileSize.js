/* @flow */

export default (
  value: number,
  {
    cutoff = 0.5,
    multiplier = 1024,
    precision = 2,
    prefixesBig = ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'],
    prefixesSmall = ['', 'm', 'u', 'n', 'p', 'f', 'a', 'z', 'y'],
    unit = 'B'
  }: {
    cutoff?: number,
    multiplier?: number,
    precision?: number,
    prefixesBig?: Array<string>,
    prefixesSmall?: Array<string>,
    unit?: string
  } = {}
) => {
  const sign = value < 0 ? '-' : '';
  let idx = 0;
  let val = value;
  if (value < 1) {
    while (val < multiplier * cutoff) {
      val *= multiplier;
      idx++;
    }
    return `${sign}${val.toFixed(precision)} ${prefixesSmall[idx]}${unit}`;
  }

  while (val > multiplier * cutoff) {
    val /= multiplier;
    idx++;
  }
  return `${sign}${val.toFixed(precision)} ${prefixesBig[idx]}${unit}`;
};
