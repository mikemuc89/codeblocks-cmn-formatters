/* @flow */
const OPTIONS = [
  'Very weak',
  'Weak',
  'Medium',
  'Strong',
  'Very strong'
];

export default (
  value: ?number,
  {
    defaultValue = 'Set password'
  }: {
    defaultValue?: string
  } = {}
) => 
  value === null ? defaultValue : OPTIONS[Math.min(value, OPTIONS.length - 1)];
